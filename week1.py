import random

def name_to_number(name):
    """ changes strings to numbers """
    
    if(name=="rock"):
        return 0
    elif(name=="Spock"):
        return 1
    elif(name=="paper"):
        return 2
    elif(name=="lizard"):
        return 3
    elif(name=="scissors"):
        return 4
    else:
        return "input not in the game"
    
    


def number_to_name(number):
    """ changes number to strings """
    
    if(number==0):
        return "rock"
    elif(number==1):
        return "Spock"
    elif(number==2):
        return "paper"
    elif(number==3):
        return "lizard"
    elif(number==4):
        return "scissors"
    else:
        return "Number not in list"
    
    

def rpsls(player_choice):
    """ takes player choice as input """
    print
    print "Player chooses",player_choice
    player_number=name_to_number(player_choice)
    comp_number=random.randrange(0,5)
    comp_choice=number_to_name(comp_number)
    print "Computer chooses",comp_choice
    if((comp_number-player_number)%5 in (1,2)):
       print "Computer wins!"
    elif((comp_number-player_number)%5 in (3,4)):
       print "Player wins!"
    else:
        print "Player and computer tie!"
    
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")





