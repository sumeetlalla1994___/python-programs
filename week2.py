import simplegui
import random
import math

def new_game():
   global secret_number
   global low
   global high
   global n
   print "New game. Range is from" ,low ,"-",high
   secret_number=random.randrange(low,high)
   n=math.ceil(math.log((high-low+1),2))
   print "no of remaining guesses",n
   print " "
    
def range100():
    global low,high
    low=0
    high=100
    new_game()

def range1000():
    global low
    global high
    low=0
    high=1000
    new_game()
    
    
def input_guess(guess):
    global secret_number
    global n
    g=int(guess)
    print "Guess was",g
    n=n-1
    print "no of  remaining guesses is",n
    if(n<=0):
        if(g>secret_number):
             print "You ran out of guesses.The number was",secret_number
             print " "
             new_game()
        elif(g<secret_number):
             print "You ran out of guesses.The number was",secret_number
             print " "
             new_game()
        else:
             print "Correct!"
             print " "
             new_game()
    else:
        if(g>secret_number):
             print "Lower!"
             print " "
        elif(g<secret_number):
             print "Higher!"
             print " "
        else:
             print "Correct!"
             print " "
             new_game()
    
    
frame = simplegui.create_frame('Testing', 200, 200)
button1 = frame.add_button('Range is [0,100)', range100,200)
button2 = frame.add_button('Range is [0,1000)', range1000,200)
inp = frame.add_input('Enter a guess', input_guess,200)
frame.start()


low=0
high=100

new_game() 