# CodeSkulptor runs Python programs in your browser.
# Click the upper left button to run this simple demo.

# CodeSkulptor runs in Chrome 18+, Firefox 11+, and Safari 6+.
# Some features may work in other browsers, but do not expect
# full functionality.  It does NOT run in Internet Explorer.

import simplegui

#message = "Welcome!"
t=0
y=0
x=0
# Handler for mouse click
 #def click():
    #global message
    #message = "Good job!"
    

    
    

# Handler to draw on canvas
def draw(canvas):
    global t
    string=format(t)
    scoe="Score"+":"+score()
    total="No of stops"+":"+chance()
    canvas.draw_text(string, [90,112], 48, "Red")
    canvas.draw_text(scoe, [220,21], 24, "Red")
    canvas.draw_text(total, [1,21], 24, "Red")

def tick():
   global t
   t=t+1

def start():
    timer.start()

def stop():
    global t
    global y
    global x
    if(timer.is_running()):
        if(t%10==0):
            x=x+1
            y=y+1
        else:
            y=y+1
            
    timer.stop()
    
def score():
    global x
    global y
    return str(x)

def chance():
    global y
    return str(y)
         
def reset():
    global t
    global x
    global y
    timer.stop()
    t=0
    x=0
    y=0

def format(t):
   
    if(t<100):
        d=t%10
        c=t//10
        if(c in range(10)):
            return "0"+":"+"0"+str(c)+"."+str(d)
    elif(t>=100):
        d=t%10
        c=t//10
        if(c>=60):
            a=c//60
            c=c%60
            if(c in range(10)):
                return str(a)+":"+"0"+str(c)+"."+str(d)
            else:
                return str(a)+":"+str(c)+"."+str(d)
        else:
                 return "0"+":"+str(c)+"."+str(d)
            
    
    
    
    
  

# Create a frame and assign callbacks to event handlers
frame = simplegui.create_frame("Home", 300, 200)
button1 = frame.add_button('Start', start)
button2 = frame.add_button('Stop', stop)
button3 = frame.add_button('Reset', reset)

frame.set_draw_handler(draw)
timer=simplegui.create_timer(100,tick);

# Start the frame animation
frame.start()
