import simplegui
import random
state=0
turn=0
a=[]
b=[]
c=[]
exposed=[]
p=0
q=0
    
# helper function to initialize globals
def new_game():
    global exposed,turn,state,a,b,c,p,q
    
    
    a=[]
    b=[]
    c=[]
    exposed=[]
    p=0
    q=0
    state=0
    turn=0
    l.set_text("Turns : "+str(turn))
        
    for i in xrange(8):
        t=(i+1)%8
        a.append(t)
    for i in xrange(8):
        t=i%8
        b.append(t)
    c=a+b
    random.shuffle(c)
    for i in xrange(16):
        t=0
        exposed.append(t)
    frame.set_draw_handler(draw)
    
    
# define event handlers
def mouseclick(pos):
    global c,exposed,state,turn,p,q
    # add game state logic here
    for i in xrange(16):
            if((pos[0]>0+1*i*50) and (pos[0]<=50+1*i*50)):
                if(not(exposed[i]))and (state==0):
                    print c[i]
                    exposed[i]=1
                    state=1
                    p=i
                    
                elif(not(exposed[i]))and (state==1):
                    print c[i]
                    exposed[i]=1
                    turn=turn+1
                    l.set_text("Turns ="+str(turn))
                    state=2
                    q=i
                    
                    
                
                else:
                    if(not(exposed[i]))and (state==2):
                                exposed[i]=1
                                print c[i]
                                #turn=turn+1
                                #l.set_text("Turns ="+str(turn))
                                if(c[p]==c[q]):
                                        exposed[p]=1
                                        exposed[q]=1
                                        p=i
                                        state=1
                            
                                else:
                                        exposed[p]=0
                                        exposed[q]=0
                                        state=1
                                        p=i
               
                            

    
                            
                        
                
            
           
                
            
                
                
    
                        
# cards are logically 50x100 pixels in size    
def draw(canvas):
    
    global c,exposed
    
    for i in xrange(16):
        if(exposed[i]):
            canvas.draw_text(str(c[i]), (25+1*i*50, 50), 20, 'White')
        else:
       
            canvas.draw_polygon([(0+50*i,0), (0+50*i, 99), (50+50*i, 99), (50+50*i, 0)], 2, "Red","Green")
        
        
       
    


# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
l = frame.add_label("Turns = 0")



# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
new_game()
frame.start()


# Always remember to review the grading rubric



