import simplegui
import random

# load card sprite - 936x384 - source: jfitz.com
CARD_SIZE = (72, 96)
CARD_CENTER = (36, 48)
card_images = simplegui.load_image("http://storage.googleapis.com/codeskulptor-assets/cards_jfitz.png")

CARD_BACK_SIZE = (72, 96)
CARD_BACK_CENTER = (36, 48)
card_back = simplegui.load_image("http://storage.googleapis.com/codeskulptor-assets/card_jfitz_back.png")    

# initialize some useful global variables
in_play = False
outcome = ""
score = 0

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}
flag=False

# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas,x,y,faceDown):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [x + CARD_CENTER[0], y + CARD_CENTER[1]], CARD_SIZE)
        if faceDown == True:
            card_loc = (CARD_BACK_CENTER[0], CARD_BACK_CENTER[1])
            canvas.draw_image(card_back, card_loc, CARD_BACK_SIZE, [x + CARD_BACK_CENTER[0], y + CARD_BACK_CENTER[1]], CARD_SIZE)
            
# define hand class
class Hand:
    def __init__(self):
        c=list()
        self.h=c	# create Hand object

    def __str__(self):
         p=" "
         for i in xrange(len(self.h)):
            p+=str(self.h[i])
         return p
                # return a string representation of a hand

    def add_card(self, card):
        self.h.append(card)	# add a card object to a hand
        
            
    def get_value(self):
        global flag
        last=0
        sum=0
        # count aces as 1, if the hand has an ace, then add 10 to hand value if it doesn't bust
        for i in xrange(len(self.h)):
            last+=1
            sum+=VALUES[str(self.h[i])[-1]]
            if(str(self.h[i])[-1]=='A'):
                flag=True
            else:
                flag=False
        
        if(flag):
            if(sum<=21):
                sum+=10
            else:
                sum+=1
            
        else:
            if(sum>21):
                if(last == len(self.h)):
                    return sum
                #else:
                    #sum=sum-VALUES[str(self.h[i])[-1]]
        return sum
 # compute the value of the hand, see Blackjack video
   
    def draw(self, canvas,y):
            # draw a hand on the canvas, use the draw method for cards
                 for card in self.h:
                        card.draw(canvas, 50+80*self.h.index(card),y, False)
        
 
        
# define deck class 
class Deck:
    def __init__(self):
        deck=[]
         # create a Deck object
        for i in xrange(len(SUITS)):
            for j in xrange(len(RANKS)):
                k=Card(SUITS[i],RANKS[j])
                deck.append(k)
        self.d=deck

    def shuffle(self):
        # shuffle the deck 
        random.shuffle(self.d)   # use random.shuffle()

    def deal_card(self):
         return random.choice((self.d))# deal a card object from the deck
    
    def __str__(self):
        o=" " # return a string representing the deck
        for i in xrange(len(self.d)):
            o+=str(self.d[i])
            
        return o
       

       



#define event handlers for buttons
def deal():
    global outcome, in_play,player,dealer,d1,score,current
    
    if in_play:
        score -= 1
    # your code goes here
    outcome=" "
    current = "Hit or Stand?"
    d1=Deck()
    player=Hand()
    dealer=Hand()
    d1.shuffle()

    
        
    player.add_card(d1.deal_card())
    player.add_card(d1.deal_card())
    dealer.add_card(d1.deal_card())
    dealer.add_card(d1.deal_card())
            
        
    
    in_play = True

def hit():
        # replace with your code below
   global outcome,score,in_play,player,dealer,current
   if in_play==True:
            player.add_card(d1.deal_card())
            if player.get_value() > 21: 
                outcome="You are busted"
                in_play=False
                current = 'New deal?'
                score=score-1
            
   
    # if the hand is in play, hit the player
        
            
            if player.get_value() == 21:
                in_play = False
                outcome = 'You got BLACKJACK!'
                current = 'New deal?'
            #print outcome
                score += 1
    # if busted, assign a message to outcome, update in_play and score
       
def stand():
        # replace with your code below
        
    global outcome,in_play,score,player,dealer,current
    if in_play==True:
        while(dealer.get_value()<17):
            dealer.add_card(d1.deal_card())
            if(dealer.get_value()>21):
                
                outcome="The dealer busted"
                score=score+1
        
        if((dealer.get_value()<=21) and dealer.get_value()>player.get_value()):
                
                outcome="Dealer wins"
                score=score-1
        elif((dealer.get_value()<=21) and dealer.get_value()<player.get_value()):
                outcome=" "
                outcome="Player wins"
                score=score+1
        elif not dealer.get_value<21 and dealer.get_value() == player.get_value():
                outcome
                outcome="It's a tie."
   
    in_play=False
    current = 'New deal?'
    # assign a message to outcome, update in_play and score

# draw handler    
def draw(canvas):
    # test to make sure that card.draw works, replace with your code below
    
    
   
    canvas.draw_text("Score: "+str(score), (400, 100), 26, "Black")
    canvas.draw_text("Blackjack", (70,100), 38, "Blue")
    canvas.draw_text("Dealer", (70, 180), 26, "Black")
    canvas.draw_text(outcome, (220, 180), 26, "Black")
    canvas.draw_text(current, (220, 380), 26, "Black")
    dealer.draw(canvas,200)
    player.draw(canvas,400)

    card = Card("S", "A")
    if in_play:
        card.draw(canvas, 50, 200, faceDown = True)
    
# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)

d1=Deck()
player=Hand()
dealer=Hand()

# get things rolling
deal()
frame.start()
